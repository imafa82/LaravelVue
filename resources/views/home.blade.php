@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <card title="Dashbord">
                    <div class="row">
                        <div class="col-md-6">
                            <card  title="Utente Migliore" background="success">
                                Massimiliano Salerno
                            </card>
                        </div>
                        <div class="col-md-6">
                            <card title="Informazioni Utente" background="success">
                                <p>
                                    Luogo di Nascita: Roma
                                </p>
                                <p>
                                    Età: 35 anni
                                </p>
                            </card>
                        </div>

                    </div>

                </card>
            </div>
        </div>
    </div>
@endsection


